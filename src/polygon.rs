use cgmath;
use glium;
use std;

use cgmath::{InnerSpace, MetricSpace, Vector2, Vector4};
use glium::Surface;

use std::convert::From;

use traits::{Collide, Draw, Friction, Init, Inside, Outside, SpatialOrder, Update, Vel};

use context::{DrawContext, RunContext};
use std::cmp::Ordering;
use std::cmp::Ordering::*;

#[derive(Copy, Clone)]
pub struct Vertex {
    pub in_pos: [f32; 2],
}
implement_vertex!(Vertex, in_pos);

pub struct Polygon<T> {
    pub pos: Vector2<T>,
    pub vel: Vector2<T>,
    pub acc: Vector2<T>,

    pub dir: T,
    pub rot: T,
    pub boo: T,

    pub vertices: Vec<Vector2<T>>,
    pub vertex_buffer: Option<glium::VertexBuffer<Vertex>>,
    pub bbrad_in: T,
    pub bbrad_out: T,

    pub frag: T,
    pub fri: T,

    pub col: Vector4<f32>,
}

impl<T> Default for Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn default() -> Self {
        Self {
            pos: Vector2::new(T::zero(), T::zero()),
            vel: Vector2::new(T::zero(), T::zero()),
            acc: Vector2::new(T::zero(), T::zero()),

            dir: T::zero(),
            rot: T::zero(),
            boo: T::zero(),

            vertices: vec![],
            vertex_buffer: None,

            bbrad_out: T::zero(),
            bbrad_in: T::infinity(),

            frag: T::one(),
            fri: T::zero(),

            col: Vector4::new(0.0, 0.0, 0.0, 0.5),
        }
    }
}

impl<T> Draw for Polygon<T>
where
    f32: From<T>,
    T: cgmath::BaseFloat,
{
    fn draw(&self, ctx: &mut DrawContext) {
        use cgmath::conv::*;
        let uniforms = uniform! {
            pos: [Into::<f32>::into(self.pos.x),
                  Into::<f32>::into(self.pos.y)],
            col: array4(self.col),
            angle: Into::<f32>::into(self.dir),
            dir: [ctx.view.dir.cos(),-ctx.view.dir.sin()],
            size: array2(ctx.view.size),
            view: array2(ctx.view.pos),
            zoom: Into::<f32>::into(ctx.view.zoom),
        };

        let uniforms_line = uniform! {
            pos: [Into::<f32>::into(self.pos.x),
                  Into::<f32>::into(self.pos.y)],
            col: [0.0f32,0.0,0.0,1.0],
            angle: Into::<f32>::into(self.dir),
            size: array2(ctx.view.size),
            view: array2(ctx.view.pos),
            zoom: Into::<f32>::into(ctx.view.zoom),
        };

        let params = glium::DrawParameters {
            blend: glium::Blend::alpha_blending(),
            line_width: Some(4.0),
            ..Default::default()
        };

        if let Some(ref buffer) = self.vertex_buffer {
            ctx.target
                .draw(
                    buffer,
                    glium::index::NoIndices(glium::index::PrimitiveType::TriangleFan),
                    &ctx.polygon_program,
                    &uniforms,
                    &params,
                )
                .unwrap();

            ctx.target
                .draw(
                    buffer,
                    glium::index::NoIndices(glium::index::PrimitiveType::LineLoop),
                    &ctx.polygon_program,
                    &uniforms_line,
                    &params,
                )
                .unwrap();
        }
    }
}

impl<T> SpatialOrder for Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn front(&self, other: &Self) -> Ordering {
        if self.pos.x - self.bbrad_out < other.pos.x - other.bbrad_out {
            Less
        } else if self.pos.x - self.bbrad_out > other.pos.x - other.bbrad_out {
            Greater
        } else {
            Equal
        }
    }
    fn back(&self, other: &Self) -> Ordering {
        if self.pos.x + self.bbrad_out < other.pos.x - other.bbrad_out {
            Less
        } else if self.pos.x + self.bbrad_out > other.pos.x - other.bbrad_out {
            Greater
        } else {
            Equal
        }
    }
}

trait Rotate<T> {
    fn rotate(self, rot: T) -> Self;
}

impl<T> Rotate<T> for cgmath::Vector2<T>
where
    T: cgmath::BaseFloat,
{
    fn rotate(self, rot: T) -> Self {
        self * rot.cos() + Vector2::new(self.y, -self.x) * rot.sin()
    }
}

impl<T> Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn accelerate(&mut self, vec: Vector2<T>) {
        self.acc += vec * self.frag;
    }
    fn accelerate2(a: &mut Self, b: &mut Self, vec: Vector2<T>) {
        let disvec = b.pos - a.pos;
        let sidvec = Vector2::new(disvec.y, -disvec.x);
        let dis = disvec.magnitude();
        let dis2 = disvec.magnitude2();

        let mfac = T::one() / (a.frag + b.frag);
        let v = vec * mfac;
        a.accelerate(v);
        b.accelerate(-v);

        if dis2 > T::zero() {
            let v = -sidvec.dot(v) / dis2 / mfac;
            //a.boost(v);
            //b.boost(-v);
        }
    }
    fn boost(&mut self, val: T) {
        self.boo += val * self.frag;
    }
    fn boost2(a: &mut Self, b: &mut Self, val: T) {
        let disvec = b.pos - a.pos;
        //let sidvec = Vector2::new(disvec.y,-disvec.x)/T::zero().acos();
        let sidvec = Vector2::new(disvec.y, -disvec.x);

        let mfac = T::one() / (a.frag + b.frag);
        let v = val * mfac;

        //println!{"v{:?} sidvec{:?}", v, sidvec};

        a.boost(v);
        b.boost(-v);

        let v = sidvec * v;

        let vc = v * mfac;
        let av = vc * b.frag;
        let bv = vc * a.frag;

        a.accelerate(av);
        b.accelerate(bv - v);
    }

    fn touch_vector(&self, other: &Self) -> Vector2<T> {
        let mut dis = (T::infinity(), Vector2::new(T::zero(), T::zero()));

        let len = other.vertices.len();
        for i in 0..len {
            let p1 = other.vertices[i].rotate(other.dir) + other.pos;
            let p2 = other.vertices[(i + 1) % len].rotate(other.dir) + other.pos;
            let line = p2 - p1;
            let normal = Vector2::new(line.y, -line.x).normalize();
            let (dis_new, vec_new) = self.vertices.iter().fold(
                (T::zero(), Vector2::new(T::zero(), T::zero())),
                |old, p_out| {
                    let vec = p_out.rotate(self.dir) + self.pos - p1;
                    let dis = vec.dot(normal);
                    if dis > old.0 {
                        let disvec = normal * dis;
                        (dis, -disvec)
                    } else {
                        old
                    }
                },
            );
            if dis_new < dis.0 {
                dis.0 = dis_new;
                dis.1 = vec_new
            }
        }

        let len = self.vertices.len();
        for i in 0..len {
            let p1 = self.vertices[i].rotate(self.dir) + self.pos;
            let p2 = self.vertices[(i + 1) % len].rotate(self.dir) + self.pos;
            let line = p2 - p1;
            let normal = Vector2::new(line.y, -line.x).normalize();
            let (dis_new, vec_new) = other.vertices.iter().fold(
                (T::zero(), Vector2::new(T::zero(), T::zero())),
                |old, p_out| {
                    let vec = p_out.rotate(other.dir) + other.pos - p1;
                    let dis = vec.dot(normal);
                    if dis > old.0 {
                        let disvec = normal * dis;
                        (dis, disvec)
                    } else {
                        old
                    }
                },
            );
            if dis_new < dis.0 {
                dis.0 = dis_new;
                dis.1 = vec_new
            }
        } /**/

        dis.1
    }

    fn inner_touch_vector(&self, other: &Self) -> Vector2<T> {
        let mut dis = (T::zero(), Vector2::new(T::zero(), T::zero()));

        let len = other.vertices.len();
        for i in 0..len {
            let p1 = other.vertices[i].rotate(other.dir) + other.pos;
            let p2 = other.vertices[(i + 1) % len].rotate(other.dir) + other.pos;
            let line = p2 - p1;
            let normal = Vector2::new(line.y, -line.x).normalize();
            dis = self.vertices.iter().fold(dis, |old, p_out| {
                let p = p_out.rotate(self.dir) + self.pos;
                let vec = p - p1;
                let dis = -vec.dot(normal);
                if dis > T::zero() {
                    let new = if vec.dot(line) < T::zero() {
                        (vec.magnitude(), -vec)
                    } else {
                        let vec2 = p - p2;
                        if vec2.dot(line) > T::zero() {
                            (vec2.magnitude(), -vec2)
                        } else {
                            let disvec = normal * dis;
                            (dis, disvec)
                        }
                    };
                    if new.0 < old.0 {
                        old
                    } else {
                        new
                    }
                } else {
                    old
                }
            });
        }

        dis.1
    }
}

impl<T> Collide for Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn collide(ball: &mut Self, ball0: &mut Self) {
        let disvec = ball0.pos - ball.pos;
        let dis = disvec.magnitude();
        if T::zero() < dis && !ball0.outside(ball) {
            let vec = ball.touch_vector(ball0);
            let force = vec; //(T::one()+T::one()).powi(8);
            Self::accelerate2(ball, ball0, force);
        }
    }

    fn collide_parent(&mut self, parent: &mut Self) {
        let child = self;
        if !child.inside(parent) {
            let force = -child.inner_touch_vector(parent);
            Self::accelerate2(parent, child, force);
        }
    }
}

impl<T> Init for Polygon<T>
where
    f32: std::convert::From<T>,
    T: cgmath::BaseFloat,
{
    fn init(&mut self, display: &glium::Display) {
        for vertex in self.vertices.iter() {
            let vertices: Vec<_> = self
                .vertices
                .iter()
                .map(|vertex| Vertex {
                    in_pos: [Into::<f32>::into(vertex.x), Into::<f32>::into(vertex.y)],
                })
                .collect();

            self.vertex_buffer =
                Some(glium::vertex::VertexBuffer::new(display, &vertices).unwrap());
            let dis = vertex.magnitude();
            if self.bbrad_out < dis {
                self.bbrad_out = dis
            }
            if self.bbrad_in > dis {
                self.bbrad_in = dis
            }
        }
    }
}

impl<T> Inside for Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn inside(&self, other: &Self) -> bool {
        /*
        if self.pos.distance(other.pos) + self.bbrad_out < other.bbrad_in {
            return true
        }*/
        //println!{"real inside"}
        let len = other.vertices.len();
        for i in 0..len {
            let p1 = other.vertices[i].rotate(other.dir) + other.pos;
            let p2 = other.vertices[(i + 1) % len].rotate(other.dir) + other.pos;
            let line = p2 - p1;
            let normal = Vector2::new(line.y, -line.x);
            let back = self.vertices.iter().all(|p_out| -> bool {
                let vec = p_out.rotate(self.dir) + self.pos - p1;
                vec.dot(normal) > T::zero()
            });
            if !back {
                return false;
            }
        }
        true
    }
}

impl<T> Outside for Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn outside(&self, other: &Self) -> bool {
        let dis = self.pos.distance(other.pos);
        if dis > self.bbrad_out + other.bbrad_out {
            return true;
        }
        /*
        if dis + self.bbrad_out < other.bbrad_in {
            return false
        }
        if dis + other.bbrad_out < self.bbrad_in {
            return false
        }*/
        //println!{"real outside"}
        //println!{"real outside {:?} + {:?} = {:?}", self.bbrad_out, other.bbrad_out, self.bbrad_out+other.bbrad_out}
        let len = other.vertices.len();
        for i in 0..len {
            let p1 = other.vertices[i].rotate(other.dir) + other.pos;
            let p2 = other.vertices[(i + 1) % len].rotate(other.dir) + other.pos;
            let line = p2 - p1;
            let normal = Vector2::new(line.y, -line.x);
            let front = self.vertices.iter().all(|p_out| -> bool {
                let vec = p_out.rotate(self.dir) + self.pos - p1;
                vec.dot(normal) < T::zero()
            });
            if front {
                return true;
            }
        }
        false
    }
}

/*
impl<T> Smaller for Polygon<T> where T: cgmath::BaseFloat {
    fn smaller(&self, other: &Self) -> bool {
        self.rad<other.rad
    }
}*/

impl<T> Friction for Polygon<T>
where
    T: cgmath::BaseFloat,
{
    fn friction(&mut self, object: &mut Self) {
        //self.vel = Vector2::new(T::zero(),T::zero());
        //object.vel = Vector2::new(T::zero(),T::zero());
        //self.rot = T::zero();
        //object.rot = T::zero();
        let disvec = object.pos - self.pos;
        let sidvec = Vector2::new(disvec.y, -disvec.x);
        let dis2 = disvec.magnitude2();

        let dvel = object.vel - self.vel;
        let drot = object.rot - self.rot;
        let srot = object.rot + self.rot;
        let srot = object.rot * self.frag + self.rot * object.frag;

        let mfac = T::one() / (self.frag + object.frag);

        let tfac = T::one() / (T::one() + T::one());
        let tfac = mfac;
        //let tfac = T::zero();

        let force = (dvel - sidvec * srot * tfac) * object.fri;
        //self.accelerate(force*mfac);
        //object.accelerate(-force*mfac);
        Self::accelerate2(self, object, force);

        let torce = if dis2 > T::zero() {
            sidvec.dot(dvel) / dis2
        } else {
            T::zero()
        };

        let torque = (drot) * object.fri;
        self.boost(torque * mfac);
        object.boost(-torque * mfac);
        //Self::boost2(self, object, torque);
    }
}

impl<T> Vel for Polygon<T>
where
    T: Copy,
{
    type Type = Vector2<T>;
    fn vel(self) -> Self::Type {
        self.vel
    }
}

impl<T> Update for Polygon<T>
where
    T: cgmath::BaseNum,
{
    fn update(&mut self) {
        self.vel += self.acc;
        self.pos += self.acc;
        self.pos += self.vel;
        self.acc = Vector2::new(T::zero(), T::zero());

        self.rot += self.boo;
        self.dir += self.boo;
        self.dir += self.rot;
        self.boo = T::zero();
    }
}

pub fn make_viewfollow<T>(fac: f32) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    f32: std::convert::From<T>,
    T: cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, _parent, ctx| {
        ctx.view.pos += Vector2::new(
            Into::<f32>::into(child.pos.x),
            Into::<f32>::into(child.pos.y),
        ) * fac;
        ctx.view.pos /= 1.0 + fac;
    })
}

pub fn make_rotfollow<T>(fac: f32) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    f32: std::convert::From<T>,
    T: cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, _parent, ctx| {
        use std::f32::consts::PI;
        let cdir: f32 = child.dir.into();
        let rdir: f32 = cdir - ctx.view.dir;
        let rdir = (rdir + PI) % (2.0 * PI) - PI;
        ctx.view.dir += rdir * (1.0 - fac)
    })
}

pub fn make_controller<T>(fac: T) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    T: From<f32> + cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, parent, ctx| {
        let state = ctx.state;
        let force = Vector2::<T>::new(state.XL.into(), state.YL.into()) * fac * fri_acc(parent.fri);
        Polygon::accelerate2(child, parent, force);
    })
}

pub fn make_controller_rot<T>(
    fac: T,
    rfac: T,
) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    T: From<f32> + cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, parent, ctx| {
        let state = ctx.state;
        let sin = child.dir.sin();
        let cos = child.dir.cos();
        let lr: T = state.XL.into();
        let ud: T = state.YL.into();
        let force = Vector2::new(ud * sin, ud * cos) * fac * fri_acc(parent.fri);
        let torque = lr * rfac * fri_acc(parent.fri);
        Polygon::accelerate2(child, parent, force);
        Polygon::boost2(child, parent, torque);
    })
}

pub fn make_controller_auto<T>(
    fac: T,
    rfac: T,
) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    T: From<f32> + cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, parent, ctx| {
        let state = ctx.state;
        let sin = child.dir.sin();
        let cos = child.dir.cos();
        let lr: T = state.XL.into();
        let ud: T = state.YL.into();
        if lr == T::zero() && ud == T::zero() {
            return;
        }
        let (vsin, vcos) = ctx.view.dir.sin_cos();
        let goal_dir = Vector2::new(
            lr * vcos.into() + ud * vsin.into(),
            ud * vcos.into() - lr * vsin.into(),
        );
        let goal_rot = goal_dir * cos - Vector2::new(goal_dir.y, -goal_dir.x) * sin;
        let front = -Vector2::new(sin, cos);
        let rdir = (goal_rot.x).atan2(goal_rot.y);
        let racc = rdir.signum() * rfac.min(rdir.abs());
        let dot = front.dot(goal_dir).min(T::zero());
        let force = front * dot * fac * fri_acc(parent.fri);
        let torque = racc * fri_acc(parent.fri);
        Polygon::accelerate2(child, parent, force);
        Polygon::boost2(child, parent, torque);
    })
}

pub fn make_controller2<T>(fac: T) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    T: From<f32> + cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, parent, ctx| {
        let state = ctx.state;
        let force = Vector2::<T>::new(state.XR.into(), state.YR.into()) * fac * fri_acc(parent.fri);
        Polygon::accelerate2(child, parent, force);
    })
}

pub fn make_controller2_rot<T>(
    fac: T,
    rfac: T,
) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    T: From<f32> + cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, parent, ctx| {
        let state = ctx.state;
        let sin = child.dir.sin();
        let cos = child.dir.cos();
        let lr: T = state.XR.into();
        let ud: T = state.YR.into();
        let force = Vector2::new(ud * sin, ud * cos) * fac * fri_acc(parent.fri);
        let torque = lr * rfac * fri_acc(parent.fri);
        Polygon::accelerate2(child, parent, force);
        Polygon::boost2(child, parent, torque);
    })
}

pub fn make_controller2_auto<T>(
    fac: T,
    rfac: T,
) -> Box<Fn(&mut Polygon<T>, &mut Polygon<T>, &mut RunContext)>
where
    T: From<f32> + cgmath::BaseFloat + 'static,
{
    fn fri_acc<T: cgmath::BaseFloat>(fri: T) -> T {
        T::one() / (T::one() - fri) - T::one()
    }
    Box::new(move |child, parent, ctx| {
        let state = ctx.state;
        let sin = child.dir.sin();
        let cos = child.dir.cos();
        let lr: T = state.XR.into();
        let ud: T = state.YR.into();
        if lr == T::zero() && ud == T::zero() {
            return;
        }
        let (vsin, vcos) = ctx.view.dir.sin_cos();
        let goal_dir = Vector2::new(
            lr * vcos.into() + ud * vsin.into(),
            ud * vcos.into() - lr * vsin.into(),
        );
        let goal_rot = goal_dir * cos - Vector2::new(goal_dir.y, -goal_dir.x) * sin;
        let front = -Vector2::new(sin, cos);
        let rdir = (goal_rot.x).atan2(goal_rot.y);
        let racc = rdir.signum() * rfac.min(rdir.abs());
        let dot = front.dot(goal_dir).min(T::zero());
        let force = front * dot * fac * fri_acc(parent.fri);
        let torque = racc * fri_acc(parent.fri);
        Polygon::accelerate2(child, parent, force);
        Polygon::boost2(child, parent, torque);
    })
}
