use context::{DrawContext, RunContext};
use traits::{
    Collide,
    //Smaller,
    Draw,
    Friction,
    Init,
    Inside,
    Outside,
    SpatialOrder,
    Update,
};

use std::cmp::Ordering;

use glium;

pub struct Floor<T> {
    pub object: T,
    pub children: Vec<Floor<T>>,
    pub rim_children: Vec<Floor<T>>,
    pub child_actions: Vec<Box<Fn(&mut T, &mut T, &mut RunContext)>>,
    pub parent_actions: Vec<Box<Fn(&mut T, &mut T, &mut RunContext)>>,
    pub actions: Vec<Box<Fn(&mut T, &mut T, &mut RunContext)>>,
    pub solid_outside: bool,
    pub solid_inside: bool,
}

impl<F> Default for Floor<F>
where
    F: Default,
{
    fn default() -> Self {
        Self {
            object: F::default(),
            children: Vec::new(),
            rim_children: Vec::new(),
            actions: Vec::new(),
            child_actions: Vec::new(),
            parent_actions: Vec::new(),
            solid_inside: true,
            solid_outside: true,
        }
    }
}

impl<T> Draw for Floor<T>
where
    T: Draw,
{
    fn draw(&self, ctx: &mut DrawContext) {
        self.object.draw(ctx);
        for c in self.children.iter().chain(self.rim_children.iter()) {
            c.draw(ctx)
        }
    }
}

impl<T> Floor<T>
where
    T: Init,
{
    pub fn recursive_init(&mut self, display: &glium::Display) {
        self.object.init(display);
        for child in self.children.iter_mut().chain(self.rim_children.iter_mut()) {
            child.recursive_init(display)
        }
    }
}

impl<T> Floor<T>
where
    T: Collide,
{
    fn collide(floor: &mut Self, floor0: &mut Self) {
        for child in floor.rim_children.iter_mut() {
            Self::collide(child, floor0);
        }
        for child0 in floor0.rim_children.iter_mut() {
            Self::collide(floor, child0);
            for child in floor.rim_children.iter_mut() {
                Self::collide(child, child0);
            }
        }
        T::collide(&mut floor.object, &mut floor0.object)
    }
}

/*
impl<T> Floor<T> where T: Outside {
    fn unite(floor: Self, floor0: Self) -> (Self, Option<Self>) {
        let obj = floor.object;
        let obj0 = floor0.object;
        if(obj.smaller(obj0) {
            if !obj.outside(obj0) {
                floor0.rim_children.push(floor)
            }
        }
        for child in floor.rim_children.iter_mut() {
            Self::collide(child, floor0);
        }
        for child0 in floor0.rim_children.iter_mut() {
            Self::collide(floor, child0);
            for child in floor.rim_children.iter_mut() {
                Self::collide(child, child0);
            }
        }
    }
}
*/

impl<T> Floor<T>
where
    T: Inside + Outside + Collide + Friction + SpatialOrder,
{
    fn front(a: &Self, b: &Self) -> Ordering {
        SpatialOrder::front(&a.object, &b.object)
    }

    fn back(a: &Self, b: &Self) -> Ordering {
        SpatialOrder::back(&a.object, &b.object)
    }

    pub fn act(&mut self, ctx: &mut RunContext) {
        let Self {
            ref mut object,
            ref mut children,
            ref mut rim_children,
            ref child_actions,
            solid_inside,
            ..
        } = *self;

        let (rim_new, new) = rim_children
            .drain(0..)
            .partition(|child| child.object.inside(object));
        *rim_children = rim_new;
        children.extend(new);

        children.sort_unstable_by(Self::front);
        rim_children.sort_unstable_by(Self::front);

        for i in 0..children.len() {
            if let Some((child, rest)) = children[i..].split_first_mut() {
                for child0 in rest {
                    if Self::back(child, child0) == Ordering::Less {
                        break;
                    }
                    Self::collide(child, child0);
                }
            }
        }
        for i in 0..rim_children.len() {
            if let Some((child, rest)) = rim_children[i..].split_first_mut() {
                for child0 in rest {
                    if Self::back(child, child0) == Ordering::Less {
                        break;
                    }
                    Self::collide(child, child0);
                }
            }
        }
        for child in children.iter_mut() {
            for child0 in rim_children.iter_mut() {
                if Self::back(child, child0) == Ordering::Less {
                    break;
                }
                Self::collide(child, child0);
            }
        }

        if solid_inside {
            for child in children.iter_mut() {
                child.object.collide_parent(object);
            }
        }

        for child in children.iter_mut().chain(rim_children.iter_mut()) {
            child.act(ctx);

            let Self {
                object: ref mut child,
                ref parent_actions,
                ..
            } = *child;

            child.friction(object);

            for action in child_actions.iter() {
                action(child, object, ctx)
            }

            for action in parent_actions.iter() {
                action(child, object, ctx)
            }
        }

        let (rim_new, new) = children
            .drain(0..)
            .partition(|child| !child.object.inside(object));
        *children = new;
        rim_children.extend(rim_new);
    }
}

impl<T> Update for Floor<T>
where
    T: Update,
{
    fn update(&mut self) {
        self.object.update();
        for mut c in self.children.iter_mut().chain(self.rim_children.iter_mut()) {
            c.update()
        }
    }
}
