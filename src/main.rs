#[macro_use]
extern crate glium;
extern crate cgmath;
extern crate gilrs;
extern crate num_traits;

use cgmath::{InnerSpace, Vector2, Vector4};

use num_traits::float::Float;

mod context;
mod floor;
mod traits;
//mod circle;
mod polygon;
mod shader;

use context::{DrawContext, RunContext, State, View};

use traits::{Draw, Update};

use floor::Floor;

use polygon::{
    make_controller, make_controller2, make_controller2_auto, make_controller2_rot,
    make_controller_auto, make_controller_rot, make_rotfollow, make_viewfollow, Polygon,
};

//use circle::Circle;

use glium::Surface;

fn main() {
    use gilrs::{Event, Gilrs};

    let mut gilrs = Gilrs::new().unwrap();

    let mut state = State::default();
    let mut view = View {
        zoom: 64.0,
        size: Vector2::new(1920, 1080),
        dir: 0.0,
        pos: Vector2::new(0.0, 0.0),
    };

    let mut events_loop = glium::glutin::EventsLoop::new();

    let window =
        glium::glutin::WindowBuilder::new().with_dimensions((view.size.x, view.size.y).into());

    let context = glium::glutin::ContextBuilder::new();

    let display = glium::Display::new(window, context, &events_loop).unwrap();

    let mut closed = false;

    let wsize = 2.0.powi(9);

    let ssize = 2.0.powi(4);

    let mut ship = Floor {
        object: Polygon {
            col: Vector4::new(0.75, 0.25, 0.0, 1.0),
            fri: 1.0 / 2.0.powi(2),
            frag: 2.0.powi(0),
            vertices: vec![
                Vector2::new(-ssize, -ssize),
                Vector2::new(-ssize, ssize),
                Vector2::new(0.0, 2.0 * ssize),
                Vector2::new(ssize, ssize),
                Vector2::new(ssize, -ssize),
                Vector2::new(0.0, -2.0 * ssize),
            ],
            ..Default::default()
        },
        children: vec![Floor {
            object: Polygon {
                col: Vector4::new(0.0, 1.0, 0.0, 0.5),
                vertices: vec![
                    Vector2::new(0.0, 1.0),
                    Vector2::new(1.0, -1.0),
                    Vector2::new(-1.0, -1.0),
                ],
                frag: 2.0.powi(8),
                ..Default::default()
            },
            parent_actions: vec![
                make_controller2_auto(0.25, 0.1),
                make_viewfollow(1.0 / 2.0.powi(2)),
            ],
            ..Default::default()
        }],
        parent_actions: vec![
            make_controller_rot(4.0, 0.02),
            make_viewfollow(1.0 / 2.0.powi(4)),
            make_rotfollow(1.0 / 2.0.powi(4)),
        ],
        ..Default::default()
    };

    let dn = 4;

    let range = -(ssize as i16) / dn..(ssize as i16) / dn;

    for x in range.clone() {
        for y in range.clone() {
            let pos = Vector2::new((dn / 2 + x * dn) as f32, (dn / 2 + y * dn) as f32);
            if pos.magnitude() > 2.0 {
                let child = Floor {
                    object: Polygon {
                        pos,
                        col: Vector4::new(0.0, 0.5, 1.0, 0.5),
                        vertices: vec![
                            Vector2::new(0.0, 1.0),
                            Vector2::new(1.0, 0.0),
                            Vector2::new(0.0, -1.0),
                            Vector2::new(-1.0, 0.0),
                        ],
                        frag: 2.0.powi(8),
                        ..Default::default()
                    },
                    ..Default::default()
                };
                ship.children.push(child)
            }
        }
    }

    let mut floor = Floor {
        object: Polygon {
            col: Vector4::new(0.0, 0.0, 1.0, 0.5),
            //*
            vertices: vec![
                Vector2::new(-wsize, -wsize),
                Vector2::new(-wsize, wsize),
                Vector2::new(wsize, wsize),
                Vector2::new(wsize, -wsize),
            ], //*/
            frag: 2.0.powi(-16),
            fri: 1.0 / 2.0.powi(8),
            ..Default::default()
        },
        children: vec![ship],
        child_actions: vec![],
        ..Default::default()
    };

    /*
    let range = -(wsize as i16)/32..(wsize as i16)/32;

    for x in range.clone() {
        for y in range.clone() {
            let pos = Vector2::new((x*16) as f32,(y*16) as f32);
            if pos.magnitude() > 32.0 {
                let child = Floor {
                    object: Polygon {
                        pos,
                        col: Vector4::new(0.0,0.5,1.0,0.5),
                        vertices: vec![
                            Vector2::new(0.0,1.0),
                            Vector2::new(1.0,0.0),
                            Vector2::new(0.0,-1.0),
                            Vector2::new(-1.0,0.0),
                        ],
                        frag: 10.0,
                        ..Default::default()
                    },
                    ..Default::default()
                };
                floor.children.push(child)
            }
        }
    }
    */
    floor.recursive_init(&display);

    let mut floors = vec![floor];

    let circle_program = glium::Program::from_source(
        &display,
        shader::circle::VS_CODE,
        shader::circle::FS_CODE,
        Some(shader::circle::GS_CODE),
    )
    .unwrap();

    let polygon_program = glium::Program::from_source(
        &display,
        shader::polygon::VS_CODE,
        shader::polygon::FS_CODE,
        None,
    )
    .unwrap();

    while !closed {
        use std::time;

        let now = time::Instant::now();

        for floor in floors.iter_mut() {
            {
                let mut ctx = RunContext {
                    view: &mut view,
                    state: &state,
                };
                floor.act(&mut ctx);
            }
            floor.update();

            let mut target = display.draw();
            target.clear_color(0.0, 0.0, 0.0, 1.0);
            {
                let mut ctx = DrawContext {
                    target: &mut target,
                    display: &display,
                    circle_program: &circle_program,
                    polygon_program: &polygon_program,
                    view: &view,
                };
                floor.draw(&mut ctx)
            }
            target.finish().unwrap();
        }

        //state = State::default();

        while let Some(Event { id, event, time }) = gilrs.next_event() {
            use gilrs::EventType::*;
            match event {
                ButtonPressed(button, _) => {
                    //println!{"{:?}", button}
                    use gilrs::Button::*;
                    match button {
                        RightThumb => {
                            state.XR = 0.0;
                            state.YR = 0.0;
                        }
                        LeftThumb => {
                            state.XL = 0.0;
                            state.YL = 0.0;
                        }
                        _ => (),
                    }
                }
                AxisChanged(axis, val, _) => {
                    //println!{"{:?}", axis}
                    use gilrs::Axis::*;
                    match axis {
                        RightStickX => state.XR = val,
                        RightStickY => state.YR = val,
                        LeftStickX => state.XL = val,
                        LeftStickY => state.YL = val,
                        LeftZ => state.YR = 1.0 - 2.0 * val,
                        RightZ => state.XR = -(1.0 - 2.0 * val),
                        _ => (),
                    }
                }
                _ => (),
            }
            //println!("New event from {}: {:?}", id, event);
        }
        events_loop.poll_events(|event| {
            if let glium::glutin::Event::WindowEvent { event, .. } = event {
                use glium::glutin::WindowEvent::*;
                match event {
                    CloseRequested => closed = true,
                    Resized(size) => {
                        view.size = Vector2::new(size.width as u32, size.height as u32)
                    }
                    KeyboardInput {
                        input:
                            glium::glutin::KeyboardInput {
                                virtual_keycode: Some(code),
                                state: key_state,
                                ..
                            },
                        ..
                    } => {
                        use glium::glutin::ElementState::*;
                        let _pressed = match key_state {
                            Pressed => true,
                            Released => false,
                        };
                        use glium::glutin::VirtualKeyCode::*;
                        match code {
                            Escape => closed = true,
                            _ => (),
                        }
                    }
                    _ => (),
                }
            }
        });
        {
            use std::thread;
            use std::time::Duration;

            let fps = 30;

            let wait = Duration::from_millis(1000 / fps);

            let elapsed = now.elapsed();

            if wait > elapsed {
                thread::sleep(wait - elapsed);
            } else {
                println! {"Low performance!"};
            }
            let nanos = now.elapsed().subsec_nanos();
            //let fps = 1000000000/nanos;
            //println!{"fps = {}", fps};
        }
    }
}
