use glium;
use std::cmp::Ordering;

use context::DrawContext;

pub trait SpatialOrder {
    fn front(&self, &Self) -> Ordering;
    fn back(&self, &Self) -> Ordering;
}

pub trait Draw {
    fn draw(&self, &mut DrawContext);
}

pub trait Collide {
    fn collide(&mut Self, &mut Self);
    fn collide_parent(&mut self, &mut Self);
}

pub trait Smaller {
    fn smaller(&self, &Self) -> bool;
}

pub trait Outside {
    fn outside(&self, &Self) -> bool;
}

pub trait Inside {
    fn inside(&self, &Self) -> bool;
}

pub trait Friction {
    fn friction(&mut self, &mut Self);
}

pub trait Vel {
    type Type;
    fn vel(self) -> Self::Type;
}

pub trait Init {
    fn init(&mut self, &glium::Display);
}

/*

pub trait Distance<T> {
    type Other=Self;
    fn distance(&self, &Other) -> T;
    fn distance2(&self, &Other) -> T;
}

/*
impl<T: Float, D> Distance<T> for D {
    fn distance(&self, other: &Other) -> T {
        distance2(self, other).sqrt()
    }
}
*/

pub trait Touch<T>: Distance<T> {
    fn touch<T>
}
*/

pub trait Update {
    fn update(&mut self);
}

pub trait Accelerate<T> {
    fn accelerate(&mut self, T);
    fn accelerate2(a: &mut Self, b: &mut Self, v: T);
}
