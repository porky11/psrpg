use std;
use cgmath;
use glium;

use glium::Surface;
use cgmath::{
    Vector2,
    Vector4,
    MetricSpace,
    InnerSpace
};

use traits::{
    Collide,
    Draw,
    Inside,
    Outside,
    Smaller,
    Update,
    Accelerate,
};
use context::{
    DrawContext,
    State
};

pub struct Circle<T> {
    pub rad: T,
    pub pos: Vector2<T>,
    pub vel: Vector2<T>,
    pub acc: Vector2<T>,
    
    pub frag: T,
    pub fri: T,
    
    pub col: Vector4<f32>,

}

impl<T> Draw for Circle<T> where f32: std::convert::From<T>, T: cgmath::BaseFloat {
    fn draw(&self, ctx: &mut DrawContext) {
        let uniforms = uniform! {
            pos: [Into::<f32>::into(self.pos.x),
                  Into::<f32>::into(self.pos.y)],
            rad: Into::<f32>::into(self.rad),
            col: cgmath::conv::array4(self.col),
            size: cgmath::conv::array2(ctx.view.size),
            view: cgmath::conv::array2(ctx.view.pos),
            zoom: Into::<f32>::into(ctx.view.zoom),
        };
        
        let params = glium::DrawParameters {
            blend: glium::Blend::alpha_blending(),
            .. Default::default()
        };

        ctx.target.draw(glium::vertex::EmptyVertexAttributes{len: 1},
                        glium::index::NoIndices(glium::index::PrimitiveType::Points),
                        &ctx.circle_program, &uniforms, &params).unwrap();
    }
}


impl<T> Circle<T> where T: cgmath::BaseFloat {
    fn frag(&self) -> T {
        self.frag/(self.rad*self.rad)
    }
}

impl<T> Accelerate<Vector2<T>> for Circle<T> where T: cgmath::BaseFloat {
    
    fn accelerate(&mut self, vec: Vector2<T>) {
        self.acc+=vec*self.frag();
    }
    fn accelerate2(a: &mut Self, b: &mut Self, vec: Vector2<T>) {
        let mfac = T::one()/(child.frag()+parent.frag());
        let v = vec*mfac;
        a.accelerate(v);
        b.accelerate(-v);
    }
}

impl<T> Collide for Circle<T> where T: cgmath::BaseFloat {
        
    fn collide(ball: &mut Self, ball0: &mut Self) {
        let disvec = ball0.pos - ball.pos;
        let dis = disvec.magnitude();
        let srad = ball.rad + ball0.rad;
        
        if T::zero()<dis && !ball0.outside(ball) {
            let dir = disvec/dis;
            let acc = srad - dis;
            let force = -dir*acc;
            Self::accelerate2(ball, ball0, force);
        }
    }
    
    fn collide_parent(&mut self, parent: &mut Self) {
        let child = self;
        let disvec = parent.pos-child.pos;
        let dis = disvec.magnitude();
        let drad = parent.rad-child.rad;
        if drad<dis {
            let dir = disvec/dis;
            let acc = drad-dis;
            let force = dir*acc*mfac;
            Self::accelerate2(parent, child, force);
        }
    }
}


impl<T> Default for Circle<T> where T: cgmath::BaseFloat {
    fn default() -> Self {
        Self {
            rad: T::one(),
            pos: Vector2::new(T::zero(), T::zero()),
            vel: Vector2::new(T::zero(), T::zero()),
            acc: Vector2::new(T::zero(), T::zero()),
            
            frag: T::one(),
            fri: T::zero(),

            col: Vector4::new(0.0,0.0,0.0,0.5),
        }
    }
}

impl<T> Inside for Circle<T> where T: cgmath::BaseFloat {
    fn inside(&self, other: &Self) -> bool {
        let dis = self.pos.distance(other.pos);
        let drad = other.rad-self.rad;
        dis<drad
    }
}

impl<T> Outside for Circle<T> where T: cgmath::BaseFloat {
    fn outside(&self, other: &Self) -> bool {
        let dis = self.pos.distance(other.pos);
        let srad = other.rad+self.rad;
        srad<dis
    }
}

impl<T> Smaller for Circle<T> where T: cgmath::BaseFloat {
    fn smaller(&self, other: &Self) -> bool {
        self.rad<other.rad
    }
}

impl<T> Update for Circle<T> where T: cgmath::BaseNum {
    fn update(&mut self) {
        self.vel+=self.acc;
        self.pos+=self.acc;
        self.pos+=self.vel;
        self.acc = Vector2::new(T::zero(), T::zero());
    }
}        

pub fn make_controller<T>(fac: T) -> Box<Fn(&mut Circle<T>, &mut Circle<T>, &State)>
    where T: cgmath::BaseFloat+'static {
    fn fri_acc<T: cgmath::BaseFloat> (fri: T) -> T {
        T::one()/(T::one()-fri)-T::one()
    }
    Box::new(
        move |child, parent, state| {
            fn num<T: cgmath::BaseFloat>(a: bool) -> T {if a {T::one()} else {T::zero()}};
            let force = Vector2::new(
                num::<T>(state.right)-num::<T>(state.left),
                num::<T>(state.up)-num::<T>(state.down)
            )*fac*fri_acc(parent.fri);
            Circle::accelerate2(child, parent, force);
        }
    )
}

pub fn make_friction<T>() -> Box<Fn(&mut Circle<T>, &mut Circle<T>, &State)>
    where T: cgmath::BaseFloat+'static {
    Box::new(
        move |child, parent, _| {
            let dvel = parent.vel - child.vel;
            let force = dvel*mfac*parent.fri;
            Circle::accelerate2(child, parent, force);
        }
    )
}

