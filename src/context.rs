use glium::{Display, Frame, Program};

use cgmath::Vector2;

pub struct State {
    pub XL: f32,
    pub YL: f32,

    pub XR: f32,
    pub YR: f32,
}

impl Default for State {
    fn default() -> Self {
        Self {
            XL: 0.0,
            YL: 0.0,

            XR: 0.0,
            YR: 0.0,
        }
    }
}

pub struct View {
    pub pos: Vector2<f32>,
    pub zoom: f32,
    pub dir: f32,
    pub size: Vector2<u32>,
}

pub struct RunContext<'a, 'b> {
    pub state: &'a State,
    pub view: &'b mut View,
}

pub struct DrawContext<'a, 'b, 'c, 'd> {
    pub target: &'a mut Frame,
    pub display: &'b Display,
    pub polygon_program: &'c Program,
    pub circle_program: &'c Program,
    pub view: &'d View,
}
